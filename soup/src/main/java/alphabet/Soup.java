package alphabet;

import java.util.ArrayList;
import java.util.Iterator;

public class Soup {
  
  private int r = 0;
  private int c = 0;
  
  private char[][] matrix;
  
  private ArrayList<WordCoordinates> words = new ArrayList<WordCoordinates>();
  
  public Soup() {
  }
  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  public Soup(int a, int b){
    r = a;
    c = b;
    matrix = new char[r][c];
  }
  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  public void addChar2Matrix(int r, int c, char x) {
    matrix[r][c] = x;
  }
  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  public void printMatrixWithSpaces(){
    for (int r=0; r<matrix.length; r++) {
      for (int c=0; c<matrix[0].length; c++) {
        System.out.print(""+matrix[r][c]+" ");
      }
      System.out.println();
    }
  }
  

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  public void addString(String s) {
    words.add(new WordCoordinates(s));
  }
  

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  public void displayWordsToFind() {
    for (int i=0; i<words.size(); i++) {
      System.out.println(((WordCoordinates)words.get(i)).getWord());
    }
  }
  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  public void displayScanResults() {
    System.out.println("OUTPUT");
    for (int i=0; i<words.size(); i++) {
      System.out.println(((WordCoordinates)words.get(i)).getWord()+" "+
                         ((WordCoordinates)words.get(i)).getCoordinates());
    }
  }
  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  public void findTheWords() {
    for (int i=0; i<words.size(); i++) {
      
      if (!((WordCoordinates)words.get(i)).getFound()) { 
        lookHorizontallyForward(words.get(i));
      }
      
      if (!((WordCoordinates)words.get(i)).getFound()) { 
        lookHorizontallyWordReverse(words.get(i));
      }
      
      if (!((WordCoordinates)words.get(i)).getFound()) { 
        lookVerticallyForward(words.get(i));
      }
      
      if (!((WordCoordinates)words.get(i)).getFound()) { 
        lookVerticallyWordReverse(words.get(i));
      }
      
      if (!((WordCoordinates)words.get(i)).getFound()) { 
        lookDiagonallyL2RDown(words.get(i));
      }
      
      if (!((WordCoordinates)words.get(i)).getFound()) { 
        lookDiagonallyL2RDownWordReverse(words.get(i));
      }
      
      if (!((WordCoordinates)words.get(i)).getFound()) {
        lookDiagonallyL2RUpWord(words.get(i));
      }
      
      if (!((WordCoordinates)words.get(i)).getFound()) {
        lookDiagonallyL2RUpWordReverse(words.get(i));
      }
      
    }
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  private void lookHorizontallyForward(WordCoordinates wc) {
    try {
    
      for (int r=0; r<matrix.length; r++) {
        String row = new String(matrix[r]);
        
        if (row.contains(wc.getWord())) {
          
          int start = row.indexOf(wc.getWord());
          // matrix indexing starts at 0, minus one to adjust
          int finish = start + wc.getWord().length() - 1;
          
          wc.setCoordinates(""+r+":"+start+" "+r+":"+finish);
          wc.setFound(true);
        }
      }
      
    } catch ( Exception ex ) {
      ex.printStackTrace();
    }
  }
  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  private void lookHorizontallyWordReverse(WordCoordinates wc) {
    try {
      for (int r=0; r<matrix.length; r++) {
        String row = new String(matrix[r]);
        
        if (row.contains(wc.getReverseWord())) {
          
          int start = row.indexOf(wc.getReverseWord());
          int finish = start + wc.getReverseWord().length() - 1; // matrix delta
          
          // when reverse, word finishes, numerically, ealier than start
          // word finish coordinates are "first"
          wc.setCoordinates(""+r+":"+finish+" "+r+":"+start);
          wc.setFound(true);
        }
      }
    } catch ( Exception ex ) {
      ex.printStackTrace();
    }
  }
  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  private void lookVerticallyForward(WordCoordinates wc) {
    try {
      for (int c=0; c<matrix[0].length; c++) {
        
        char[] columnOfChars = new char[matrix.length];
        for (int r=0; r<matrix.length; r++) {
          columnOfChars[r] = matrix[r][c];
        }
        
        String columnStr = new String(columnOfChars);
        
        if (columnStr.contains(wc.getWord())) {
          
          // column letters contains word, and starts at indexOf
          int start = columnStr.indexOf(wc.getWord());
          // new add length to start, but minus 1 for matrix index offset 
          int finish = start + wc.getWord().length() - 1;
          
          wc.setCoordinates(""+start+":"+c+" "+finish+":"+c);
          wc.setFound(true);
        }
        
      }
    } catch ( Exception ex ) {
      ex.printStackTrace();
    }
  }
  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  private void lookVerticallyWordReverse(WordCoordinates wc) {
    try {
      for (int c=0; c<matrix[0].length; c++) {
        
        char[] columnOfChars = new char[matrix.length];
        for (int r=0; r<matrix.length; r++) {
          columnOfChars[r] = matrix[r][c];
        }
        
        String columnStr = new String(columnOfChars);
        
        if (columnStr.contains(wc.getReverseWord())) {
          
          // column of letters contains reverse order contained word
          // the word starts at the bottom of this array
          int start = columnStr.indexOf(wc.getReverseWord());
          int finish = start + wc.getWord().length() - 1;
          
          wc.setCoordinates(""+finish+":"+c+" "+start+":"+c);
          wc.setFound(true);
        }
      }
    } catch ( Exception ex ) {
      ex.printStackTrace();
    }
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  private void lookDiagonallyL2RDown(WordCoordinates wc) {
    
    char[] wordCA = wc.getWord().toCharArray();
    boolean continueScanning = true;
    boolean wordFound = false;
    
    int rx = 0;
    int cx = 0;
    int wx = 0;
    
    // move back from the edge of the original matrix by the length of the word
    // only a "subMatrix" needs to be scanned
    int rowSubMatrix = matrix.length - wordCA.length;
    int colSubMatrix = matrix[0].length - wordCA.length + 1;
    
    try {
      
      while ( continueScanning ) {
        
        // a cell in the "soup" matches the first character of the word
        if ( matrix[rx][cx] == wordCA[wx] ) {
          // will check the set of words diagonal down
          wordFound = diagonalForwardDownCheck(rx,cx,wordCA);
          
          if ( wordFound ) {
            wc.setFound(wordFound);
            
            // found starting index row(rx) col(cx)
            // add word lenght to staring index
            // minus 1 to adjust for matrix starting a 0.
            int erx = rx + wordCA.length - 1;
            int ecx = cx + wordCA.length - 1;
            
            wc.setCoordinates(""+rx+":"+cx+" "+erx+":"+ecx);
            
            continueScanning = false;
          }
        }
        
        if ( cx < colSubMatrix ) {
          cx++;
        }
        
        if ( cx == colSubMatrix ) {
          rx++;
          cx = 0;
        }
        
        if ( rx == rowSubMatrix ) {
          continueScanning = false;
        }
      }
      
    } catch ( Exception ex ) {
      ex.printStackTrace();
    }

  }
  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  private void lookDiagonallyL2RDownWordReverse(WordCoordinates wc) {

    char[] wordCA = wc.getReverseWord().toCharArray();
    boolean continueScanning = true;
    boolean wordFound = false;
    
    int rx = 0;
    int cx = 0;
    int wx = 0;
    
    // move back from the edge of the matrix by the length of the word 
    int rowSubMatrix = matrix.length - wordCA.length;
    int colSubMatrix = matrix[0].length - wordCA.length + 1;
    
    try {
      
      while ( continueScanning ) {
        
        // a cell in the "soup" matches the first character of the word
        if ( matrix[rx][cx] == wordCA[wx] ) {
          // will check the set of words diagonal down
          wordFound = diagonalForwardDownCheck(rx,cx,wordCA);
          
          if ( wordFound ) {
            
            wc.setFound(wordFound);
            
            // add word length to start, and adjust for matrix indexing ( - 1 ) 
            int erx = rx + wordCA.length - 1;
            int ecx = cx + wordCA.length - 1;
            
            wc.setCoordinates(""+erx+":"+ecx+" "+rx+":"+cx);
            
            continueScanning = false;
          }
        }
        
        if ( cx < colSubMatrix ) {
          cx++;
        }
        
        if ( cx == colSubMatrix ) {
          rx++;
          cx = 0;
        }
        
        if ( rx == rowSubMatrix ) {
          continueScanning = false;
        }
      }
      
    } catch ( Exception ex ) {
      ex.printStackTrace();
    }

  }    
  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  private void lookDiagonallyL2RUpWord(WordCoordinates wc) {

    char[] wordCA = wc.getWord().toCharArray();
    boolean continueScanning = true;
    boolean wordFound = false;
    
    int rx = matrix.length - 1;  // array offset
    int cx = 0;
    int wx = 0;
    
    // this scan starts at the lower, left corner
    // move back from the far edge of starting poing, lowwer left corner,
    // the matrix by the length of the word, and define a submatrix
    // int rowSubMatrix = matrix.length - wordCA.length;
    int rowSubMatrix = wordCA.length - 1;
    int colSubMatrix = matrix[0].length - wordCA.length;
    
    try {
      while ( continueScanning ) {

        // a cell in the "soup" matches the first character of the word
        if ( matrix[rx][cx] == wordCA[wx] ) {
          // will check the set of words diagonal down
          wordFound = diagonalForwardUpCheck(rx,cx,wordCA);
          
          if ( wordFound ) {
            wc.setFound( wordFound );
            
            int erx = rx - wordCA.length + 1; //moving up rows, by length of word
            int ecx = cx + wordCA.length - 1; //moving out cols, by lenght of word
            
            wc.setCoordinates(""+rx+":"+cx+" "+erx+":"+ecx);
            
            continueScanning = false;
          }
        
        }
        
        // move over 1 colume to the right
        if ( cx < colSubMatrix ) {
          cx++;
        }
        
        // if at edge of submatrix, move up a row and back to col 0
        if ( cx >= colSubMatrix ) {
          rx--;
          cx = 0;
        }
        
        if ( rx == rowSubMatrix - 1 ) {  // may need to scan col on this row
          continueScanning = false;
        }
        
      }
    } catch ( Exception ex ) {
      ex.printStackTrace();
    }
  }
  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  private void lookDiagonallyL2RUpWordReverse(WordCoordinates wc) {

    char[] wordCA = wc.getReverseWord().toCharArray();
    boolean continueScanning = true;
    boolean wordFound = false;
    
    int rx = matrix.length - 1;  // array offset
    int cx = 0;
    int wx = 0;
    
    // this scan starts at the lower, left corner
    // move back from the far edge of starting poitng, lowwer left corner,
    // the matrix by the length of the word, and define a submatrix
    // int rowSubMatrix = matrix.length - wordCA.length;
    int rowSubMatrix = wordCA.length + 1;
    int colSubMatrix = matrix[0].length - wordCA.length;
    
    try {
      while ( continueScanning ) {

        // a cell in the "soup" matches the first character of the word
        if ( matrix[rx][cx] == wordCA[wx] ) {
          // will check the set of words diagonal down
          wordFound = diagonalForwardUpCheck(rx,cx,wordCA);
          if ( wordFound ) {
            
            wc.setFound( wordFound );
            int erx = rx - wordCA.length + 1; // moving up rows, by length of word
                                              // row counting appears correct, probably line 404
            int ecx = cx + wordCA.length - 1; // moving out cols, by length of word
                                              // minus 1 for array indexing
            wc.setCoordinates(""+cx+":"+rx+" "+ecx+":"+erx);
            
            continueScanning = false;
            
          }
        }
        
        // move over 1 colume to the right
        if ( cx < colSubMatrix ) {
          cx++;
        }
        
        // if at edge of submatrix, move up a row and back to col 0
        if ( cx >= colSubMatrix ) {
          rx--;
          cx = 0;
        }
        
        if ( rx == rowSubMatrix ) {  // may need to scan col on this row
          continueScanning = false;
        }
        
      }
    } catch ( Exception ex ) {
      ex.printStackTrace();
    }
  }
  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  private boolean diagonalForwardDownCheck(int x, int y, char[] word) {
    // take the first character
    // loop through row by row
    //    if char matches then
    //       start going diagonally
    //       if matches if found stop
    
    int ix = x;
    int iy = y;
    int wordLength = word.length;
    int wordY = 0;
    boolean result = false;
    
    try {
      
      // copy possible diabonal word into a single char array
      char[] diagWord = new char[word.length];
      int step = 0;
      while (step < word.length) {
        diagWord[step] = matrix[ix][iy];
        ix++;
        iy++;
        step++;
      }
      String s = new String(diagWord);
      
      if (s.contains(new String(word))) {
        result = true;
      }
      
    } catch ( Exception ex ) {
      ex.printStackTrace();
    }
    
    return result;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  private boolean diagonalForwardUpCheck(int x, int y, char[] word) {
    // take the first character
    // loop through row by row, starting at the bottom
    //    if char matches then
    //       start going diagonally
    //       if matches if found stop
   
    int ix = x;
    int iy = y;
    int wordLength = word.length;
    int wordY = 0;
    boolean result = false;
    
    try {

      // copy possible diabonal word into a single char array
      char[] diagWord = new char[word.length];
      int step = 0;
      while (step < word.length) {
        diagWord[step] = matrix[ix][iy];
        ix--;  // diagonal up the matrix
        iy++;  // diagonal up the matrix
        step++;// one step into the reverse word
      }
      String s = new String(diagWord);
      
      if (s.contains(new String(word))) {
        result = true;
      }
    
    } catch ( Exception ex ) {
      ex.printStackTrace();
    }
    
    return result;
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  public static void main(String[] arg) {
    Bowl bowl = new Bowl();
    bowl.parseFile(bowl.getFileName());
  }
  
}