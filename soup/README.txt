

    Enlighten's Alphabet-Soup Programming Challenge
    Hank Greene's Code

    Implemented in Java
    Managed by Maven
    Install Java (java.com), Maven (maven.apache.org) and git.

    > git clone git@gitlab.com:<   >/alphabet-soup.git 
    > cd alphabet-soup/soup
    > mvn package

    A test runs and displays the results of processing a 40x40 matrix. 
    Copy your valid "soup" file into the "soup" directory.

    > ls -l
      20x20.txt
      3x3.txt
      5x5.txt
      pom.xml
      src
      target

    Execute the following command to manually process another file.

    > java -cp target/soup-1.0-SNAPSHOT.jar alphabet.Soup
    > enter file name> 3x3.txt
      INPUT
      3x3
      A B C 
      D E F 
      G H I 
      ABC
      AEI

      OUTPUT
      ABC 0:0 0:2
      AEI 0:0 2:2
 
 
    For your_file.txt

    > java -cp target/soup-1.0-SNAPSHOT.jar alphabet.Soup
    > enter file name> your_file.txt




 
